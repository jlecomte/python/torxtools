# Contributing

## Development environment setup

### System packages

This project relies on `make` to run `Makefiles`. On a Debian-like system, you can install
`make` with the following commands:

~~~bash
sudo apt-get update
sudo apt-get install -y make
~~~

`pipx` is recommended to host python tools in their own environment, per user environment:

~~~bash
sudo apt-get update
sudo apt-get install -y pipx
~~~

It is necessary to run commands from a virtual environment. You can install the
necessary package to create a virtual environment:

~~~bash
pipx install virtualenv
~~~


### User development tools

Install build environment packages globally for your user:

~~~bash
echo bump2version sphinx pre-commit pyproject-build tomlq twine | xargs -n1 pipx install
~~~


### Virtual environment

To setup a development environment, clone the git repository, then create a Python virtual environment:

~~~bash
# create a virtualenv inside a folder called '.venv'
virtualenv .venv
source .venv/bin/activate

# Install development requirements:
python3 -mpip install --prefer-binary -U -r requirements-dev.txt
~~~


### Targets

Makefile targets can be viewed by simply typing:

~~~
make help
~~~


### Unit tests

To run unit tests, you can simply run the make target named `check`:

~~~bash
# run all tests:
make check
# or
make test
~~~

Pytest flags can be passed via the `PYTESTFLAGS` variable, either as
a command line argument, or as environment variable.

~~~bash
# Pass -x to pytest for this command
make check PYTESTFLAGS='-x'
# or
PYTESTFLAGS='-x' make check

# Pass -x to all `make check` until unset
export PYTESTFLAGS='-x'
make check
~~~


### Python version matrix tests

In order to test the repository on several Python versions at same time, you
must first install `pyenv`, and then install the required Python versions in
the `pyenv`. These python versions can be shared amongst several repositories.

You can follow the `pyenv` instruction here to install `pyenv` then the required
python versions: [https://github.com/pyenv/pyenv](https://github.com/pyenv/pyenv)

Otherwise, you can follow these slightly altered instructions assuming that `~/bin`
is in your path and that you are on a Debian-like distribution:

~~~bash
# Install build packages
sudo apt-get update
sudo apt-get install binutils build-essential

# Install minimal dependencies
sudo apt-get install libssl-dev zlib1g-dev

# Install pyenv in ~/.local/lib, next to ~/.local/lib/python3.x
mkdir -p ~/bin ~/.local/lib

git clone https://github.com/pyenv/pyenv.git ~/.local/lib/pyenv
cd ~/.local/lib/pyenv && src/configure && make -C src
ln -sf -T ~/.local/lib/pyenv ~/.pyenv
ln -sf -T ~/.local/lib/pyenv/bin/pyenv ~/bin/pyenv

for v in 3.7-dev 3.8-dev 3.9-dev 3.10-dev 3.11-dev 3.12-dev; do
  pyenv install ${v}
done

# Enable the environments
pyenv global system 3.7-dev 3.8-dev 3.9-dev 3.10-dev 3.11-dev 3.12-dev
~~~

You can then run `make`:

~~~bash
make 3.x
~~~


## Checking

### pre-commit

To properly format the repository, you can run all linting tools in one go.

~~~bash
make pre-commit
# or
make pc
~~~

If you wish for this to be automatic on every commit or push, install the pre-commit
commit hook:

~~~bash
# Choose at which stage you prefer:
pre-commit install -t pre-commit
# or
pre-commit install -t pre-push
~~~

The tools currently configured to run on `make pre-commit` (or `make pc`),
and the other tools that are not configured, can be installed independently
from `pre-commit`:

~~~bash
pipx install pre-commit-hooks

# Example command:
check-ast **/*py
~~~
