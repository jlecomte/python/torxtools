import os

from pytest import mark

from torxtools import xdgtools


@mark.parametrize(
    "key, value, expected",
    [
        # fmt: off
        ("XDG_CONFIG_HOME", None, "/home/jdoe/.config"),
        ("XDG_CACHE_HOME", None, "/home/jdoe/.cache"),
        ("XDG_DATA_HOME", None, "/home/jdoe/.local/share"),
        ("XDG_RUNTIME_DIR", None, None),
        #
        ("XDG_CONFIG_DIR", None, "/etc/xdg"),
        ("XDG_DATA_DIR", None, "/usr/local/share:/usr/share"),
        #
        ("XDG_CONFIG_HOME", "/data/$USER/cfg", "/data/jdoe/cfg"),
        ("XDG_RUNTIME_DIR", "/run/$USER", "/run/jdoe"),
        ("XDG_CONFIG_DIR", "/etc/xdg:/etc/$USER/xdg", "/etc/xdg:/etc/jdoe/xdg"),
        # fmt: on
    ],
)
def test_xdg_setenv(_noenv, key, value, expected):
    if value:
        os.environ[key] = value

    xdgtools.setenv()
    assert expected == os.environ.get(key)
