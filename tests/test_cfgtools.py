import uuid
from os.path import exists, isdir, isfile

from pytest import mark

from torxtools import cfgtools

# default return value:
DEV_NULL = "/dev/null"
# files guaranteed to exist, but isfile is False
DEV_ZERO = "/dev/zero"
DEV_FULL = "/dev/full"
# files guaranteed to exist / not exist
YES_FILE = "/etc/hostname"
NO_FILE = "/tmp/" + str(uuid.uuid4())

MIXED_LIST = ["/tmp", "$HOME", DEV_FULL, "/tmp/" + str(uuid.uuid4())]


@mark.parametrize(
    "cfgfile, candidates, expected, verify",
    [
        # fmt: off
        # if cfgfile is passed, then always return it
        (NO_FILE, None, NO_FILE, None),
        (NO_FILE, None, NO_FILE, isfile),
        (NO_FILE, None, NO_FILE, isdir),
        (NO_FILE, [], NO_FILE, None),
        (NO_FILE, [], NO_FILE, isfile),
        (NO_FILE, [], NO_FILE, isdir),
        (NO_FILE, MIXED_LIST, NO_FILE, None),
        (NO_FILE, MIXED_LIST, NO_FILE, isfile),
        (NO_FILE, MIXED_LIST, NO_FILE, isdir),
        (YES_FILE, None, YES_FILE, None),
        (YES_FILE, None, YES_FILE, isfile),
        (YES_FILE, None, YES_FILE, isdir),
        (YES_FILE, [], YES_FILE, None),
        (YES_FILE, [], YES_FILE, isfile),
        (YES_FILE, [], YES_FILE, isdir),
        (YES_FILE, MIXED_LIST, YES_FILE, None),
        (YES_FILE, MIXED_LIST, YES_FILE, isfile),
        (YES_FILE, MIXED_LIST, YES_FILE, isdir),
        # without cfgfile, return "/dev/null" if no match
        (None, None, DEV_NULL, None),
        (None, None, DEV_NULL, isfile),
        (None, None, DEV_NULL, isdir),
        (None, [], DEV_NULL, None),
        (None, [], DEV_NULL, isfile),
        (None, [], DEV_NULL, isdir),
        (None, [DEV_NULL, "/tmp"], DEV_NULL, None),
        (None, [DEV_NULL, "/tmp"], DEV_NULL, isfile),
        (None, [DEV_NULL, NO_FILE], DEV_NULL, isdir),
        # without cfgfile, return first match
        (None, MIXED_LIST, "/tmp", None),
        (None, MIXED_LIST, DEV_NULL, isfile), # no match
        (None, MIXED_LIST, "/tmp", isdir),
        (None, [*MIXED_LIST, YES_FILE], "/tmp", None),
        (None, [YES_FILE, *MIXED_LIST], YES_FILE, None),
        (None, [*MIXED_LIST, YES_FILE], "/tmp", isdir),
        (None, [YES_FILE, *MIXED_LIST], "/tmp", isdir),
        (None, [YES_FILE, *MIXED_LIST], YES_FILE, isfile),
        (None, [YES_FILE, *MIXED_LIST], YES_FILE, isfile),
        # fmt: on
    ],
)
def test_which(_noenv, cfgfile, candidates, expected, verify):
    retval = cfgtools.which(cfgfile, candidates, verify=verify or exists)
    assert retval == expected
