import argparse
from argparse import ArgumentTypeError

from pytest import mark, raises

from torxtools.argtools import is_int_negative, is_int_negative_or_zero, is_int_positive, is_int_positive_or_zero


@mark.parametrize(
    "cls,value,expected",
    # fmt: off
    [
        (is_int_positive, 1, 1),
        (is_int_positive, 0, None),
        (is_int_positive, -1, None),
        #
        (is_int_positive_or_zero, 1, 1),
        (is_int_positive_or_zero, 0, 0),
        (is_int_positive_or_zero, -1, None),
        #
        (is_int_negative, 1, None),
        (is_int_negative, 0, None),
        (is_int_negative, -1, -1),
        #
        (is_int_negative_or_zero, 1, None),
        (is_int_negative_or_zero, 0, 0),
        (is_int_negative_or_zero, -1, -1),
    ],
    # fmt: on
)
def test_int_function(cls, value, expected):
    parser = argparse.ArgumentParser()
    parser.add_argument("--dummy", action=cls, type=int)

    argv = ["--dummy", str(value)]
    if isinstance(expected, int):
        assert expected == vars(parser.parse_args(argv))["dummy"]
    else:
        with raises(ArgumentTypeError):
            parser.parse_args(argv)
