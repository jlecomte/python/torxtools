# Run black
# Since it's also a precommit hook, do not run in `all`
#
BLACKFLAGS ?=

.PHONY: black
black: ## python: format (with 'black') all files
	black -q $(BLACKFLAGS) -- \
	    $(PROJECT_DIR) $(PROJECT_EXTRA_DIRS) \
	    $(wildcard bin/*) $(wildcard tests) $(wildcard setup.py)
