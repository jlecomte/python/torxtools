# Changelog

## 1.2.1 - 2024-11-11

Added:

  - added `CHANGELOG.md`, and re-upload for pypi

## 1.2.0 - 2024-11-11

Changed:

  - added `is_file_and_not_dir` to replace `argtools.is_not_dir`. `argtools.is_not_dir` will be re-used in the future.
  - `argtools` must now be passed as `action` and no longer `type` for `argparse.add_argument()`
