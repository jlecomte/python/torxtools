[![license](https://img.shields.io/badge/license-MIT-brightgreen)](https://spdx.org/licenses/MIT.html)
[![documentation](https://img.shields.io/badge/documentation-html-informational)](https://torxtools.docs.cappysan.dev)
[![pipelines](https://gitlab.com/cappysan/torxtools/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/cappysan/torxtools/pipelines)
[![coverage](https://gitlab.com/cappysan/torxtools/badges/master/coverage.svg)](https://torxtools.docs.cappysan.dev/coverage/index.html)

# torxtools

Less generic functionalities than Phillips and Pozidriv tools. One size fits most. MacGyver's French army spork.

Quick and dirty library that holds functions used in projects with similarities.

## Installation

You can install the latest version from PyPI package repository.

~~~bash
python3 -mpip install -U torxtools
~~~


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.


## Locations

  * Documentation: [https://torxtools.docs.cappysan.dev](https://torxtools.docs.cappysan.dev)
  * Website: [https://gitlab.com/cappysan/torxtools](https://gitlab.com/cappysan/torxtools)
  * PyPi: [https://pypi.org/project/torxtools](https://pypi.org/project/torxtools)
